# coding=utf-8
"""
Realizar un programa que evalúe la función f(x)= -2**2 + (11X)/3 en el intervalor de [-10,10]
con incrementos desde 0.1,0.001,0.0001 etc.
este programa obtiene el valor más grande para cada x

"""
__autor__ = "ugarciac"
import time
import matplotlib.pyplot as plt
import math

def convert_time(num_digits,time_,operacion):
    hour = (int(time_/3600))
    minutes = (int(time_)/60)
    seconds = time_ - (hour*3600) - minutes * 60
    if hour > 23:
        days = hour / 24
    else: days = 0
    save_text(num_digits,days,hour,minutes,seconds,operacion)
    print("digits:",num_digits,"time {0:.1f}".format(hour),"h {0:.1f}".format(minutes),"m {0:.4f}".format(seconds),"s")

def plot_time(xnumber_digits,ytimes):
    plt.plot(xnumber_digits,ytimes,'c')
    plt.title("funcion en el tiempo")
    plt.ylabel("Time(s)")
    plt.xlabel("digits")
    plt.savefig("fig2.png")
    #plt.show()

def save_text(num_digits,days,hour,minutes,seconds,operacion):
    with open("time2.txt","a+") as file:
        file.write("digits: "+str(num_digits)+" time: days: "+str(days)+" {0:.0f}".format(hour)+"h:"+" {0:.0f}".format(minutes)+"m:"+" {0:.6f}".format(seconds)+"s "+
                   " max value: "+str(operacion)+"\n")

def function_in_the_time():
    divisor = 10
    start = -100
    stop = 100
    num_digits = 1
    xnumber_digits = []
    ytimes = []
    operacion = []
    while True:
        start_time = time.clock()
        for i in range(start,stop):
            number = i/divisor
            #operacion
            #operacion = max([2 * number - 3])
            operacion = max([(-2 * (math.pow(number,2))) + ((11 * number) /3)])
            #print("numero: ",number," operacion: ",operacion)
        stop_time = time.clock()
        time_ = stop_time - start_time
        #convertir tiempo (s) a horas y minutos
        convert_time(num_digits,time_,operacion)
        xnumber_digits.append(num_digits),ytimes.append(time_)
        plot_time(xnumber_digits,ytimes)

        start = start * 10
        stop  = stop * 10
        divisor = divisor * 10
        num_digits += 1
        print("max value: ",operacion)
        print("-------------------------")

if __name__ == "__main__":
    function_in_the_time()
